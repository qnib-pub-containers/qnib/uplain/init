#!/bin/ash

DPUSH="${DPUSH:=false}"

IMAGE_URI="${IMAGE_URI:=registry.gitlab.com/qnib-pub-containers/qnib/uplain/init}"
CI_COMMIT_TAG="${IMAGE_TAG:=$(date +%F)}"
ARCH="amd64|arm64"
for A in $( echo $ARCH |tr "|" " ");do
  IMG_NAME=$IMAGE_URI:$CI_COMMIT_TAG-$(echo ${A} |tr '/' '_')
  echo ">> Building '${IMG_NAME}'"
  docker buildx build ${BUILDOPTS} --platform linux/${A} --load -t $IMG_NAME .
  if [[ ${DPUSH} == "true" ]];then
    docker push $IMG_NAME
  fi
  sleep .2
done
docker buildx build --pull --platform linux/amd64,linux/arm64 --push -t $IMAGE_URI:$CI_COMMIT_TAG .

# syntax = docker/dockerfile:1.4

ARG DOCKER_REGISTRY=docker.io
ARG FROM_IMG_REPO=library
ARG FROM_IMG_NAME="ubuntu"
ARG FROM_IMG_TAG="mantic-20230511.1"
ARG FROM_IMG_HASH=""
FROM ${DOCKER_REGISTRY}/${FROM_IMG_REPO}/${FROM_IMG_NAME}:${FROM_IMG_TAG}${DOCKER_IMG_HASH}
ARG TARGETARCH
ENV ENTRYPOINTS_DIR=/opt/entry/

RUN <<eot bash
  apt update -y
  apt install -y wget file
  rm -rf /var/lib/apt/lists/*
eot
RUN <<eot bash
  wget -qO - https://gitlab.com/qnib-golang/go-github/-/releases/0.3.1/downloads/go-github_0.3.1_linux_${TARGETARCH}.tar.gz |tar xfz - -C /usr/local/bin/
  chmod +x /usr/local/bin/go-github
eot
RUN <<eot bash
  echo "# init-plain: $(/usr/local/bin/go-github rLatestUrl --ghorg qnib --ghrepo init-plain --regex 'init-plain.tar' --limit 1)"
  wget -qO - "$(/usr/local/bin/go-github rLatestUrl --ghorg qnib --ghrepo init-plain --regex 'init-plain.tar' --limit 1)" |tar xf - --strip-components=1 -C /
  wget -qO - https://gitlab.com/qnib-golang/go-fisherman/-/releases/0.1.0/downloads/go-fisherman_0.1.0_linux_${TARGETARCH}.tar.gz |tar xfz - -C /usr/local/bin/
  chmod +x /usr/local/bin/go-fisherman
  wget -qO /usr/local/bin/gosu $(/usr/local/bin/go-github rLatestUrl --ghorg tianon --ghrepo gosu --regex "gosu-${TARGETARCH}" --limit 1)
  chmod +x /usr/local/bin/gosu
eot

RUN useradd --home /home/user --shell /sbin/nologin --uid=1001 --shell /usr/sbin/nologin user
HEALTHCHECK --interval=5s --retries=5 --timeout=2s \
  CMD /usr/local/bin/healthcheck.sh
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
